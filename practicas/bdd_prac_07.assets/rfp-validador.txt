idle> @s-08-validador-main.sql
=========================================================
Iniciando validador - Practica 7
Presionar Enter si los valores configurados son correctos.
De lo contario editar el script s-08-validador-main.sql
O en su defecto proporcionar nuevos valores
=========================================================
Datos de Entrada:
1. Proporcionar el password de SYS [configurado en script]: 
1. Proporcionar el nombre de usuario [editorial_bdd]: 
2. Proporcionar password del usuario editorial_bdd [Configurado en script]: 
3. Nombre del sitio 1 (PDB 1). [rfpbd_s1]: 
4. Nombre del sitio 2 (PDB 2). [rfpbd_s2]: 
Indicar la estrategia de fragmentacion para la tabla PAGO_SUSCRIPTOR:
A - Con respecto a ARTICULO (default)
R - Con respecto a REVISTA
Indicar valor [R]: 
Iniciando validacion para rfpbd_s1






Connected.

                                                                                
Iniciando creacion de procedimientos para rfpbd_s1
Connected.

                                                                                
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
===============================================                                                                                                                                                         
Iniciando proceso de validacion de respuestas                                                                                                                                                           
Incluir en el reporte a partir de este punto                                                                                                                                                            
===============================================                                                                                                                                                         
SESSION_TIME:	19/12/2020 00:35:19
USR_COD HOST:	906224
OS_USER:	rodrigo
BD_USER:	EDITORIAL_BDD
CON_NAME:	RFPBD_S1                                                                                       

2020-12-19 00:35:20.150-906224-PL.R001R.L001E.L001 ==> OK sinonimo ARTICULO                                                                                                                             
2020-12-19 00:35:20.151-906224-CK.O002F.K002D.K002 ==> OK sinonimo ARTICULO_1                                                                                                                           
2020-12-19 00:35:20.151-906224-0S.D003P.S003I.S003 ==> OK sinonimo ARTICULO_2                                                                                                                           
2020-12-19 00:35:20.152-906224-RQ.R004B.Q004T.Q004 ==> OK sinonimo ARTICULO_REVISTA_1                                                                                                                   
2020-12-19 00:35:20.152-906224-FG.I005D.G005O.G005 ==> OK sinonimo ARTICULO_REVISTA_2                                                                                                                   
2020-12-19 00:35:20.153-906224-PD.G0060.D006R.D006 ==> OK sinonimo PAGO_SUSCRIPTOR                                                                                                                      
2020-12-19 00:35:20.154-906224-0M.O007S.M007I.M007 ==> OK sinonimo PAGO_SUSCRIPTOR_1                                                                                                                    
2020-12-19 00:35:20.155-906224-FP.R0081.P008A.P008 ==> OK sinonimo PAGO_SUSCRIPTOR_2                                                                                                                    
2020-12-19 00:35:20.157-906224-IH.O009R.H009L.H009 ==> OK sinonimo PAIS_1                                                                                                                               
2020-12-19 00:35:20.158-906224-0I.D010F.I0100.I010 ==> OK sinonimo PAIS_2                                                                                                                               
2020-12-19 00:35:20.158-906224-UR.R011P.R011B.R011 ==> OK sinonimo REVISTA_1                                                                                                                            
2020-12-19 00:35:20.159-906224-NS.I012B.S012D.S012 ==> OK sinonimo REVISTA_2                                                                                                                            
2020-12-19 00:35:20.160-906224-AA.G013D.A013D.A013 ==> OK sinonimo SUSCRIPTOR_1                                                                                                                         
2020-12-19 00:35:20.160-906224-ML.O0140.L014E.L014 ==> OK sinonimo SUSCRIPTOR_2                                                                                                                         
2020-12-19 00:35:20.161-906224-PM.R015S.M015D.M015 ==> OK sinonimo SUSCRIPTOR_3                                                                                                                         
2020-12-19 00:35:20.162-906224-CM.O0161.M016I.M016 ==> OK sinonimo SUSCRIPTOR_4                                                                                                                         
2020-12-19 00:35:20.162-906224-0F.D017R.F017T.F017 ==> OK 16 sinonimos obtenidos                                                                                                                        
2020-12-19 00:35:20.188-906224-RW.R018F.W018O.W018 ==> OK vista ARTICULO_E1                                                                                                                             
2020-12-19 00:35:20.189-906224-FX.I019P.X019R.X019 ==> OK vista ARTICULO_E2                                                                                                                             
2020-12-19 00:35:20.190-906224-PO.G020B.O020I.O020 ==> OK vista ARTICULO_REVISTA                                                                                                                        
2020-12-19 00:35:20.191-906224-0B.O021D.B021A.B021 ==> OK vista PAGO_SUSCRIPTOR_E1                                                                                                                      
2020-12-19 00:35:20.192-906224-FY.R0220.Y022L.Y022 ==> OK vista PAGO_SUSCRIPTOR_E2                                                                                                                      
2020-12-19 00:35:20.194-906224-IF.O023S.F0230.F023 ==> OK vista PAIS                                                                                                                                    
2020-12-19 00:35:20.195-906224-0F.D0241.F024B.F024 ==> OK vista REVISTA                                                                                                                                 
2020-12-19 00:35:20.196-906224-UN.R025R.N025D.N025 ==> OK vista SUSCRIPTOR                                                                                                                              
2020-12-19 00:35:20.197-906224-NW.I026F.W026D.W026 ==> OK 8 vistas encontradas                                                                                                                          
Registros encontrados empleando vistas                                                                                                                                                                  
2020-12-19 00:35:21.439-906224-AG.G027P.G027E.G027 ==> OK  Paises                : 2                                                                                                                    
2020-12-19 00:35:21.440-906224-MK.O028B.K028D.K028 ==> OK  Suscriptor            : 3                                                                                                                    
2020-12-19 00:35:21.441-906224-PF.R029D.F029I.F029 ==> OK  Articulo              : 2                                                                                                                    
2020-12-19 00:35:21.441-906224-CK.O0300.K030T.K030 ==> OK  Revista               : 2                                                                                                                    
2020-12-19 00:35:21.442-906224-0X.D031S.X031O.X031 ==> OK  Pago_suscriptor       : 2                                                                                                                    
2020-12-19 00:35:21.443-906224-RQ.R0321.Q032R.Q032 ==> OK  Articulo_revista      : 2                                                                                                                    
2020-12-19 00:35:21.444-906224-FI.I033R.I033I.I033 ==> OK  Total pdf recibo      : 7340032                                                                                                              
2020-12-19 00:35:21.444-906224-PH.G034F.H034A.H034 ==> OK  Total pdf recibo e1   : 7340032                                                                                                              
2020-12-19 00:35:21.445-906224-0O.O035P.O035L.O035 ==> OK  Total pdf recibo e2   : 7340032                                                                                                              
2020-12-19 00:35:21.446-906224-FP.R036B.P0360.P036 ==> OK  Total pdf articulo    : 3145728                                                                                                              
2020-12-19 00:35:21.447-906224-IW.O037D.W037B.W037 ==> OK  Total pdf articulo e1 : 3145728                                                                                                              
2020-12-19 00:35:21.447-906224-0V.D0380.V038D.V038 ==> OK  Total pdf articulo e2 : 3145728                                                                                                              
2020-12-19 00:35:21.448-906224-UQ.R039S.Q039D.Q039 ==> OK Validacion concluida                                                                                                                          
Iniciando validacion para rfpbd_s2






Connected.

                                                                                                                                                                                                        
Iniciando creacion de procedimientos para rfpbd_s2
Connected.

                                                                                                                                                                                                        
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
No errors.
===============================================                                                                                                                                                         
Iniciando proceso de validacion de respuestas                                                                                                                                                           
Incluir en el reporte a partir de este punto                                                                                                                                                            
===============================================                                                                                                                                                         
SESSION_TIME:	19/12/2020 00:35:22
USR_COD HOST:	906224
OS_USER:	rodrigo
BD_USER:	EDITORIAL_BDD
CON_NAME:	RFPBD_S2                                                                                       

2020-12-19 00:35:23.648-906224-PW.R001R.W001E.W001 ==> OK sinonimo ARTICULO_1                                                                                                                           
2020-12-19 00:35:23.649-906224-CS.O002F.S002D.S002 ==> OK sinonimo ARTICULO_2                                                                                                                           
2020-12-19 00:35:23.649-906224-0G.D003P.G003I.G003 ==> OK sinonimo ARTICULO_REVISTA_1                                                                                                                   
2020-12-19 00:35:23.650-906224-RO.R004B.O004T.O004 ==> OK sinonimo ARTICULO_REVISTA_2                                                                                                                   
2020-12-19 00:35:23.650-906224-FI.I005D.I005O.I005 ==> OK sinonimo PAGO_SUSCRIPTOR                                                                                                                      
2020-12-19 00:35:23.651-906224-PN.G0060.N006R.N006 ==> OK sinonimo PAGO_SUSCRIPTOR_1                                                                                                                    
2020-12-19 00:35:23.652-906224-0W.O007S.W007I.W007 ==> OK sinonimo PAGO_SUSCRIPTOR_2                                                                                                                    
2020-12-19 00:35:23.653-906224-FX.R0082.X008A.X008 ==> OK sinonimo PAIS_1                                                                                                                               
2020-12-19 00:35:23.655-906224-IK.O009R.K009L.K009 ==> OK sinonimo PAIS_2                                                                                                                               
2020-12-19 00:35:23.656-906224-0W.D010F.W0100.W010 ==> OK sinonimo REVISTA_1                                                                                                                            
2020-12-19 00:35:23.656-906224-UO.R011P.O011B.O011 ==> OK sinonimo REVISTA_2                                                                                                                            
2020-12-19 00:35:23.657-906224-NK.I012B.K012D.K012 ==> OK sinonimo SUSCRIPTOR_1                                                                                                                         
2020-12-19 00:35:23.657-906224-AM.G013D.M013D.M013 ==> OK sinonimo SUSCRIPTOR_2                                                                                                                         
2020-12-19 00:35:23.658-906224-MN.O0140.N014E.N014 ==> OK sinonimo SUSCRIPTOR_3                                                                                                                         
2020-12-19 00:35:23.659-906224-PK.R015S.K015D.K015 ==> OK sinonimo SUSCRIPTOR_4                                                                                                                         
2020-12-19 00:35:23.660-906224-CO.O0162.O016I.O016 ==> OK 15 sinonimos obtenidos                                                                                                                        
2020-12-19 00:35:23.685-906224-0D.D017R.D017T.D017 ==> OK vista ARTICULO                                                                                                                                
2020-12-19 00:35:23.686-906224-RW.R018F.W018O.W018 ==> OK vista ARTICULO_REVISTA                                                                                                                        
2020-12-19 00:35:23.687-906224-FB.I019P.B019R.B019 ==> OK vista PAGO_SUSCRIPTOR_E1                                                                                                                      
2020-12-19 00:35:23.687-906224-PV.G020B.V020I.V020 ==> OK vista PAGO_SUSCRIPTOR_E2                                                                                                                      
2020-12-19 00:35:23.689-906224-0Y.O021D.Y021A.Y021 ==> OK vista PAIS                                                                                                                                    
2020-12-19 00:35:23.690-906224-FF.R0220.F022L.F022 ==> OK vista REVISTA                                                                                                                                 
2020-12-19 00:35:23.691-906224-IU.O023S.U0230.U023 ==> OK vista SUSCRIPTOR                                                                                                                              
2020-12-19 00:35:23.692-906224-0U.D0242.U024B.U024 ==> OK 7 vistas encontradas                                                                                                                          
Registros encontrados empleando vistas                                                                                                                                                                  
2020-12-19 00:35:24.131-906224-UD.R025R.D025D.D025 ==> OK  Paises                : 2                                                                                                                    
2020-12-19 00:35:24.132-906224-NC.I026F.C026D.C026 ==> OK  Suscriptor            : 3                                                                                                                    
2020-12-19 00:35:24.132-906224-AE.G027P.E027E.E027 ==> OK  Articulo              : 2                                                                                                                    
2020-12-19 00:35:24.133-906224-MH.O028B.H028D.H028 ==> OK  Revista               : 2                                                                                                                    
2020-12-19 00:35:24.134-906224-PO.R029D.O029I.O029 ==> OK  Pago_suscriptor       : 2                                                                                                                    
2020-12-19 00:35:24.135-906224-CS.O0300.S030T.S030 ==> OK  Articulo_revista      : 2                                                                                                                    
2020-12-19 00:35:24.135-906224-0N.D031S.N031O.N031 ==> OK  Total pdf recibo      : 7340032                                                                                                              
2020-12-19 00:35:24.136-906224-RM.R0322.M032R.M032 ==> OK  Total pdf recibo e1   : 7340032                                                                                                              
2020-12-19 00:35:24.137-906224-FI.I033R.I033I.I033 ==> OK  Total pdf recibo e2   : 7340032                                                                                                              
2020-12-19 00:35:24.138-906224-PT.G034F.T034A.T034 ==> OK  Total pdf articulo    : 3145728                                                                                                              
2020-12-19 00:35:24.138-906224-0Y.O035P.Y035L.Y035 ==> OK Validacion concluida                                                                                                                          
