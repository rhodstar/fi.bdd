## Base de datos distribuidas

```
Noviembre de 2020
Oracle 18g | Arquitectura multitenant
```

### Script de compilación de archivos latex

Para compilar los archivos `.tex` se proporciona un script, cuya finalidad es
tener un mejor orden de la generación de los archivos intermedios de la
compilación de LaTeX

```sh
./compile.sh <nombreArchivo>
```

No es necesario indicar la extensión `.tex`
